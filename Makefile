TARGET := c++-structure-tree
SOURCES := src/tree.cc src/parser.cc
CXX ?= g++

CXXFLAGS += `llvm-config --cxxflags`
CXXFLAGS += -std=c++11 -Wall -pedantic -Isrc/ -MMD -MP
LDFLAGS += `llvm-config --ldflags`
LDFLAGS += -lclang

default: $(TARGET)

$(TARGET): $(SOURCES)
	$(CXX) $(CXXFLAGS) -o $(@) $(filter %.cc, $(^)) $(LDFLAGS)

-include $(TARGET).d

.PHONY: clean

clean:
	rm $(TARGET)
	rm $(TARGET).d
	rm -r doc

doc:
	doxygen
