
struct AA {
  int a, b;
  float c;
};

struct A {
  int x;
  AA thing;
  bool bl;
  struct XY {
    void * p;
  };
};

namespace N1 { namespace N2 {
  struct S1 {
    struct S2 {
      struct S3 {
        int DATA;
      };
    };
  };
} }


template<typename T> struct Templated {
  T * pointer;
  struct InsideT {
    void * p;
  } inner;
};
template<typename T> struct Templated<T*> {
  T not_a_pointer;
  struct InsideTPtr {
    void * p;
  };
  InsideTPtr inner;
};

template<> struct Templated<bool> {
  bool completely_different;
  char than_the_others;
  struct InsideTB {
    void * p;
  };
};

struct ContainsTemplate {
  Templated<int> an_int;
  Templated<int*> an_int_ptr;
};

struct STR { int i; };
typedef STR * STRP;
typedef STR STR_rename;
struct containsSTRP {
  STRP ptr;
  STR_rename data;
};

