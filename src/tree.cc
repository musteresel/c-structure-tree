#include <iostream>
#include "parser.hh"

/** Indent by the given number of levels.
 *
 * This function prepares the given stream, so that the next
 * output line will appear with the specified indentation.
 * */
void indent_by(std::ostream & out, std::size_t level) {
  for (auto i = 0u; i < level; ++i) {
    out << "|   ";
  }
}

std::string collect_scopes(parser::Structure const & structure) {
  std::string scope;
  for (auto s : structure.scopes()) {
    scope = s.name() + "::" + scope;
  }
  return scope;
}
std::string collect_namespaces(parser::Structure const & structure) {
  std::string space;
  for (auto n : structure.namespaces()) {
    space = n + "::" + space;
  }
  return space;
}

/** Recursively print the contents of structures.
 *
 * This function prints the member (names) of a structure. If a member
 * is a structure, then its contents are also printed, etc.
 * */
void print_contents(
    std::ostream & out,
    parser::Structure const & structure,
    std::size_t level = 0) {
  for (auto member : structure.members()) {
    if (not member.isField()) {
      continue;
    }
    indent_by(out, level);
    auto type = member.type();
    out << "|-- " << member.name() << " : " << type.name() << "\n";
    if (type.isStructure()) {
      print_contents(out, type.getStructure(), level + 1);
    }
  }
}

/** Parse a file and print a tree of the defined structures.
 *
 * @todo Proper error handling.
 * */
int main(int argc, char ** argv) {
  try {
    auto parsedFile = parser::parse(argc - 1, argv + 1);
    for (auto structure : parsedFile.structures()) {
      std::cout
        << collect_namespaces(structure) << collect_scopes(structure)
        << structure.name() << "\n";
      print_contents(std::cout, structure);
      std::cout << "\n";
    }
    return 0;
  }
  catch (...) {
    std::cerr << "oops" << std::endl;
    return 1;
  }
}

