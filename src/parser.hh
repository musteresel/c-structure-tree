/** @file Interface to the C++ source code parser.
 *
 * This header defines the interface used by the rest of the code to access
 * the C++ source code parser. Following YAGNI, it contains only what is
 * currently needed and will be extended when the need arises.
 *
 * @warning Follows YAGNI, not a stable interface.
 * */
#ifndef _PARSER_HH_
#define _PARSER_HH_ 1
#include <clang-c/Index.h>
#include <string>
#include <vector>
#include <set>

/** Parser logic is packed into the 'parser' namespace. */
namespace parser {
  // Forward declarations
  struct Type;
  struct Member;
  struct Structure;
  struct ParsedFile;

  /** Represents a structure or class from the C++ source code.
   *
   * Objects of this class represent structures and classes found by the
   * parser in the C++ source code. They allow querying for various
   * properties of the structure / class.
   *
   * @warning Instances may become invalid when the parser::ParsedFile object
   * gets invalidated!
   * */
  class Structure {
    protected:
      CXCursor const cursor;

    public:
      /** Constructor - do not use. */
      Structure(CXCursor);

      /** Get the name of the structure or class.
       *
       * Returns the name or spelling of the structure / class represented
       * by this object.
       *
       * @returns Name of structure / class.
       * */
      std::string name(void) const;

      /** Get the members of the structure or clas.
       *
       * Returns a vector of members of the structure / class represented
       * by this object. Members in this sense include both member fields
       * and member functions.
       *
       * @returns Members of structure / class.
       * */
      std::vector<Member> members(void) const;

      /** Get all namespaces this structure / class is contained in.
       *
       * Returns a vector of namespaces names, starting with the inner most.
       *
       * @warning This is highley likely subject to future changes.
       * */
      std::vector<std::string> namespaces(void) const;

      /** Get all names of enclosing structure / class declarations.
       *
       * Return a vector of structure / class names, starting with the inner
       * most.
       *
       * @warning This is highely likely subject to future changes.
       * */
      std::vector<Structure> scopes(void) const;

      std::vector<Structure> bases(void) const;
      bool operator<(Structure const &) const;
  };

  /** Represents a type found in the C++ source code.
   *
   * Objects of this class represent the types of parser::Member objects and
   * allow basic access to their properties.
   *
   * @warning Instances may become invalid when the parser::ParsedFile object
   * gets invalidated!
   * */
  class Type {
    protected:
      CXType const type;

    public:
      /** Constructor - do not use. */
      Type(CXType);

      /** Return a string representation of this type.
       * */
      std::string name(void) const;

      /** Determines whether this object represents a structure / class type.
       * */
      bool isStructure(void) const;

      /** Get the associated parser::Structure object iff this object
       * represents a structure / class type.
       * */
      Structure getStructure(void) const;
  };

  /** Represents a member of a structure or class.
   *
   * Objects of this class represent members - both member fields and
   * member functions - of a specific parser::Structure. Basic properties
   * can be accessed through the provided interface.
   *
   * @warning Instances may become invalid when the parser::ParsedFile object
   * gets invalided!
   * */
  class Member {
    protected:
      CXCursor cursor;

    public:
      /** Constructor - do not use. */
      Member(CXCursor);

      /** Gets the name of the member as specified in the C++ source code.
       * */
      std::string name(void) const;

      /** Checks whether this member is a member field.
       * */
      bool isField(void) const;

      /** Get the associated type of this member.
       * */
      Type type(void) const;
  };

  /** Represents the main file that got parsed.
   *
   * Objects of this class are produced by the parser::parse function and
   * contain the state required to perfom queries about the contained
   * structures etc.
   *
   * @warning Invalidating an object of this class may invalidate all
   * associated objects from different classes such as parser::Structure, ...
   * */
  class ParsedFile {
    protected:
      CXIndex index;

      CXTranslationUnit unit;

    public:
      /** Constructor - do not use. */
      ParsedFile(CXIndex, CXTranslationUnit);

      /** Copy construction is not allowed. */
      ParsedFile(ParsedFile const &) = delete;

      /** Move construction is allowed.
       *
       * The moved from object is thereafter invalid. Previously with it
       * associated objects from different classes such as parser::Structure
       * are now associated with this newly constructed object.
       * */
      ParsedFile(ParsedFile &&);

      /** Copy assignment is not allowed. */
      ParsedFile & operator=(ParsedFile const &) = delete;

      /** Move assignment is allowed.
       *
       * This is implemented as a swap operation, so both state and thereafter
       * associated objects are swaped between the two objects.
       * */
      ParsedFile & operator=(ParsedFile &&);

      /** Destructor. */
      ~ParsedFile();

      /** Returns the structures that were found in the file represented by
       * this object.
       *
       * This returns only the structures that were defined in the main file
       * of the translation unit.
       * */
      std::set<Structure> structures(void) const;
  };

  /** Let the parser parse a file.
   *
   * This functions invokes the parser with the given arguments and returns
   * a parser::ParsedFile object. Among the given arguments has to be the
   * name of the file to be parsed. The arguments are passed directly to
   * the parser, so everything that ther parser understands can be passed
   * through this function (include paths, defines, ...).
   * */
  ParsedFile parse(std::size_t, char const * const *);
}
#endif
