#include "parser.hh"
namespace parser {
  /** Consume a CXString and return a std::string with its contents.
   *
   * @warning Do not use the passed CXString after this function!
   * */
  std::string to_string(CXString str) {
//    CXString str = clang_getCursorSpelling(c);
    std::string result = clang_getCString(str);
    clang_disposeString(str);
    return result;
  }

  static bool isStructureKind(CXCursorKind kind) {
    return
      (kind == CXCursor_StructDecl) or
      (kind == CXCursor_ClassDecl) or
      (kind == CXCursor_ClassTemplate) or
      (kind == CXCursor_ClassTemplatePartialSpecialization);
  }


  CXChildVisitResult
    extract_members(
      CXCursor c,
      CXCursor p,
      CXClientData cd
      ) {
    auto & accum = *static_cast<std::vector<Member> *>(cd);
    auto kind = clang_getCursorKind(c);
    if ((kind == CXCursor_FieldDecl) or (kind == CXCursor_CXXMethod)) {
      accum.emplace_back(c);
    }
    return CXChildVisit_Continue;
  }
  Structure::Structure(CXCursor c) : cursor(c) {
    // TODO Assert
  }
  std::vector<Member> Structure::members() const {
    std::vector<Member> gathered_members;
    clang_visitChildren(cursor, extract_members, &gathered_members);
    return gathered_members;
  }
  std::string Structure::name() const {
    return to_string(clang_getCursorDisplayName(cursor));
  }
  std::vector<std::string> Structure::namespaces() const {
    std::vector<std::string> gathered_namespaces;
    auto parent = clang_getCursorSemanticParent(cursor);
    auto kind = clang_getCursorKind(parent);
    while (not clang_isTranslationUnit(kind)) {
      if (kind == CXCursor_Namespace) {
        gathered_namespaces.emplace_back(
            to_string(clang_getCursorSpelling(parent)));
      }
      parent = clang_getCursorSemanticParent(parent);
      kind = clang_getCursorKind(parent);
    }
    return gathered_namespaces;
  }
  /**
   * @todo How about enclosing templated classes?
   * */
  std::vector<Structure> Structure::scopes() const {
    std::vector<Structure> gathered_scopes;
    auto parent = clang_getCursorSemanticParent(cursor);
    auto kind = clang_getCursorKind(parent);
    while (not clang_isTranslationUnit(kind)) {
      if (isStructureKind(kind)) {
        gathered_scopes.emplace_back(parent);
      }
      parent = clang_getCursorSemanticParent(parent);
      kind = clang_getCursorKind(parent);
    }
    return gathered_scopes;
  }
  bool Structure::operator<(Structure const & s) const {
    unsigned int my_offset;
    unsigned int other_offset;
    clang_getFileLocation(
        clang_getCursorLocation(cursor),
        nullptr, nullptr, nullptr,
        &my_offset);
    clang_getFileLocation(
        clang_getCursorLocation(s.cursor),
        nullptr, nullptr, nullptr,
        &other_offset);
    return my_offset < other_offset;
  }
  CXChildVisitResult
    extract_bases(
        CXCursor c, CXCursor p, CXClientData cd) {
      std::vector<Structure> & accum =
        *static_cast<std::vector<Structure> *>(cd);
      if (clang_getCursorKind(c) == CXCursor_CXXBaseSpecifier) {
        accum.emplace_back(
            clang_getTypeDeclaration(clang_getCursorType(c))
            );
      }
      return CXChildVisit_Continue;
    }

  std::vector<Structure> Structure::bases() const {
    std::vector<Structure> gathered_bases;
    clang_visitChildren(cursor, extract_bases, &gathered_bases);
    return gathered_bases;
  }


  Member::Member(CXCursor c) : cursor(c) {
    // TODO Assert
  }
  std::string Member::name() const {
    return to_string(clang_getCursorSpelling(cursor));
  }
  bool Member::isField() const {
    return clang_getCursorKind(cursor) == CXCursor_FieldDecl;
  }
  Type Member::type() const {
    return Type(clang_getCursorType(cursor));
  }

  Type::Type(CXType t) : type(t) {}
  bool Type::isStructure() const {
    auto decl = clang_getTypeDeclaration(
        clang_getCanonicalType(type)); // Break through typedefs
    return isStructureKind(clang_getCursorKind(decl));
  }
  std::string Type::name() const {
    return to_string(clang_getTypeSpelling(type));
  }
  Structure Type::getStructure() const {
    auto decl = clang_getTypeDeclaration(type);
    //
    auto tpl = clang_getSpecializedCursorTemplate(decl);
    if (not clang_Cursor_isNull(tpl)) {
      decl = tpl;
    }
    return Structure(decl);
  }

  CXChildVisitResult extract_structures(
      CXCursor cursor,
      CXCursor parent,
      CXClientData client_data)
  {
    CXCursorKind kind = clang_getCursorKind(cursor);
    if (not
        (clang_Location_isFromMainFile(clang_getCursorLocation(cursor))
         and
         clang_isDeclaration(kind)
         and
         clang_isCursorDefinition(cursor)))
    {
      return CXChildVisit_Continue;
    }
    auto & structures =
      *static_cast<std::set<Structure> *>(client_data);
    if (isStructureKind(kind))
    {
      structures.emplace(cursor);
    }
    return CXChildVisit_Recurse;
  }
  ParsedFile::ParsedFile(CXIndex i, CXTranslationUnit u) : index(i), unit(u) {
    // TODO Assert
  }
  ParsedFile::ParsedFile(ParsedFile && p) : index(p.index), unit(p.unit) {
    p.index = nullptr;
    p.unit = nullptr;
  }
  ParsedFile & ParsedFile::operator=(ParsedFile && p) {
    std::swap(index, p.index);
    std::swap(unit, p.unit);
    return *this;
  }
  ParsedFile::~ParsedFile() {
    if (index && unit) {
      clang_disposeTranslationUnit(unit);
      clang_disposeIndex(index);
    } else if (index || unit) {
      // TODO Oh boy ...
    }
  }
  std::set<Structure> ParsedFile::structures() const {
    std::set<Structure> gathered_structures;
    clang_visitChildren(
        clang_getTranslationUnitCursor(unit),
        extract_structures,
        &gathered_structures);
    return gathered_structures;
  }


  ParsedFile parse(std::size_t argc, char const * const * argv) {
    CXIndex index = clang_createIndex(0, 1);
    CXTranslationUnit unit = clang_parseTranslationUnit(
        index, 0, argv, argc, 0, 0, 0);
    if (not unit) {
      throw "That failed hard.";
    }
    return ParsedFile(index, unit);
  }


}
