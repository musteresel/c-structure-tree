template<typename X> struct Template { int i; X x; };
template<typename T, int I> struct ForwardTemplate;
struct Forward;
struct ForwardOnly;
struct Using {
  Forward * member;
  ForwardOnly * other;
  ForwardTemplate<Forward, 42> * comp;
  Template<Forward> hm;
  Template<bool> hm2;
};
struct Forward {
  void * data;
};
struct Unrelated {
  Forward f;
};

template<int L> struct ForwardTemplate<void, L> {
  int i;
};

template<typename T, int I> struct ForwardTemplate {
  char general;
};

struct Derived : Forward
{};
